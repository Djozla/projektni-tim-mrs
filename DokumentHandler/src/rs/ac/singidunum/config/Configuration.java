package rs.ac.singidunum.config;

import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.Map;

public class Configuration {
    private String title;
    private String coreVersion;
    private String iconPath;
    private Dictionary<String, Integer> windowSize  = new Hashtable<>();
    //prepraviti posle da ima object plugin
    private ArrayList<Object> installedPlugins = new ArrayList<>();
    private String pluginsPath;
    private String context;

    public Configuration(String title, String coreVersion, String iconPath, Dictionary<String,
            Integer> windowSize, ArrayList<Object> installedPlugins, String pluginsPath, String context) {
        this.title = title;
        this.coreVersion = coreVersion;
        this.iconPath = iconPath;
        this.windowSize = windowSize;
        this.installedPlugins = installedPlugins;
        this.pluginsPath = pluginsPath;
        this.context = context;
    }

   public Integer getWidth(){
        return this.windowSize.get("width");
   }
    public Integer getHeight(){
        return this.windowSize.get("height");
    }

    public static Configuration fromMap(Map<String, Object> configurationMap) {
        String title = (String) configurationMap.get("title");
        String coreVersion = (String) configurationMap.get("core_version");
        String icon = (String) configurationMap.get("icon");
        Dictionary<String, Integer> windowSize = (Dictionary<String, Integer>) configurationMap.get("window_size");
        ArrayList<Object> installedPlugins = (ArrayList<Object>) configurationMap.get("installed_plugins");
        String pluginsPath = (String) configurationMap.getOrDefault("plugins_path", "plugins");
        String context = (String) configurationMap.getOrDefault("context", "");

        return new Configuration(title, coreVersion, icon, windowSize, installedPlugins, pluginsPath, context);
    }

    public String getTitle() {
        return title;
    }

    public String getCoreVersion() {
        return coreVersion;
    }

    public String getIconPath() {
        return iconPath;
    }

    public Dictionary<String, Integer> getWindowSize() {
        return windowSize;
    }

    public ArrayList<Object> getInstalledPlugins() {
        return installedPlugins;
    }



public String getPluginsPath() {
        return pluginsPath;
    }

    public String getContext() {
        return context;
    }
}

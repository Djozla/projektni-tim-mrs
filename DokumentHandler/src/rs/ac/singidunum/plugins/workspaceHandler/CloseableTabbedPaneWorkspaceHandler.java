package rs.ac.singidunum.plugins.workspaceHandler;

import rs.ac.singidunum.plugins.workspaceHandler.modelPodataka.Workspace;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;

public class CloseableTabbedPaneWorkspaceHandler {

    private JTree stablo;
    private ArrayList<String> putanje = new ArrayList<>();
    private  ArrayList<Workspace> probaBrisanja = new ArrayList<>();

    public CloseableTabbedPaneWorkspaceHandler(JTree stablo, ArrayList<String> putanje){
        this.stablo = stablo;
        this.putanje = putanje;

    }

    public CloseableTabbedPaneWorkspaceHandler(JTree stablo, ArrayList<String> putanje,ArrayList<Workspace> probaBrisanja){
        this.stablo = stablo;
        this.putanje = putanje;
        this.probaBrisanja= probaBrisanja;
    }
    public void addTab(JTabbedPane tabbedPane, String title) {

        // Create a close button
        JButton closeButton = new JButton("X");


        // Create a panel for the tab title with a close button
        JPanel tabTitlePanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
        tabTitlePanel.add(new JLabel(title));
        tabTitlePanel.add(closeButton);

        // Create a panel for the tab content (tree)
        JPanel panel = new JPanel(new BorderLayout());
        panel.add(new JScrollPane(stablo), BorderLayout.CENTER);
        closeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int tabIndex = tabbedPane.indexOfComponent(panel);
                if (tabIndex != -1) {

                    for(String s: putanje){
                        String [] provera;
                        if(File.separator.equals("/")){
                            provera = s.split(File.separator);
                        }else{
                            provera = s.split("\\\\");
                        }
                        if(provera[provera.length-1].equals(title+".json")){
                            putanje.remove(s);
                            Workspace.ukloniWSTab(probaBrisanja,s);

                        }

                    }

                    tabbedPane.remove(tabIndex);
                }
            }
        });

        // Add the tab to the JTabbedPane with the custom title panel
        tabbedPane.addTab(null, panel);
        tabbedPane.setTabComponentAt(tabbedPane.getTabCount() - 1, tabTitlePanel);

    }

}

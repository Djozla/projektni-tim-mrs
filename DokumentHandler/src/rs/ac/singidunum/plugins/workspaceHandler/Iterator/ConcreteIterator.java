package rs.ac.singidunum.plugins.workspaceHandler.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import rs.ac.singidunum.plugins.workspaceHandler.composite.WorkspaceTree;
import rs.ac.singidunum.plugins.workspaceHandler.modelPodataka.Document;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import rs.ac.singidunum.plugins.workspaceHandler.modelPodataka.Kolekcija;
import rs.ac.singidunum.plugins.workspaceHandler.modelPodataka.Workspace;
import rs.ac.singidunum.resources.data.formatiranjePutanja;

public class ConcreteIterator implements Iterator{



    public ConcreteIterator(){

    }

    @Override
    public Workspace citaj(String putanjaDoSpecifikacije) {
        Workspace workspace = null;
        JSONParser parser = new JSONParser();

    try (FileReader reader = new FileReader(putanjaDoSpecifikacije)) {
            // Parsiranje JSON fajla
            Object obj = parser.parse(reader);
            JSONObject jsonObject = (JSONObject) obj;
            // Čitanje podataka iz JSON objekta
            Long id = (Long) jsonObject.get("id");
            String naziv = (String) jsonObject.get("naziv");
            workspace = new Workspace(id,naziv,putanjaDoSpecifikacije);


            // Čitanje 'collections' niza
            JSONArray collectionsArray = (JSONArray) jsonObject.get("collections");

            workspace.addCollection(processCollections(collectionsArray, putanjaDoSpecifikacije));

            // Čitanje 'dokumenti' niza
            JSONArray dokumentiArray = (JSONArray) jsonObject.get("dokumenti");
            for(Object object : dokumentiArray){
                workspace.addDocument(processDocuments(formatiranjePutanja.napraviPutanju(putanjaDoSpecifikacije, object.toString())));
            }




        }  catch (FileNotFoundException e) {
        throw new RuntimeException(e);
    } catch (IOException e) {
        throw new RuntimeException(e);
    } catch (ParseException e) {
        throw new RuntimeException(e);
    }
    return workspace;
    }

    private static Kolekcija processCollections(JSONArray collections, String path){
        Kolekcija kolekcijaNode = null;
        for (Object collectionObj : collections) {
            JSONObject collection = (JSONObject) collectionObj;

            // Extract values for the current collection
            String collectionNaziv = (String) collection.get("naziv");
            kolekcijaNode = new Kolekcija(collectionNaziv);

            // Process nested collections recursively
            JSONArray nestedCollections = (JSONArray) collection.get("collections");
            kolekcijaNode.addCollection(processCollections(nestedCollections,path));

            // Process documents for the current collection
            JSONArray documents = (JSONArray) collection.get("dokumenti");
            for(Object obj: documents){
                kolekcijaNode.addDocument(processDocuments(formatiranjePutanja.napraviPutanju(path, obj.toString())));
            }

        }
        return kolekcijaNode;
    }
    private static Document processDocuments(String putanjaDoSpecifikacije) {

        return Document.ucitajDokument(putanjaDoSpecifikacije);
    }
}

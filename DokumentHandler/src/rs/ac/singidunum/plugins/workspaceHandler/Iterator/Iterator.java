package rs.ac.singidunum.plugins.workspaceHandler.Iterator;

import rs.ac.singidunum.plugins.workspaceHandler.modelPodataka.Workspace;

public interface Iterator {
    public Workspace citaj(String putanja);
}

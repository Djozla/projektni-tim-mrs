package rs.ac.singidunum.plugins.workspaceHandler;


import com.sun.source.tree.Tree;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import rs.ac.singidunum.core.ui.ExceptionHandler;
import rs.ac.singidunum.core.ui.IntegrativnaKomponenta;
import rs.ac.singidunum.core.ui.pluginManager.plugin.Plugin;
import rs.ac.singidunum.core.ui.pluginManager.plugin.PluginSpecification;

import rs.ac.singidunum.plugins.DocumentHandler.DocumentHandler;
import rs.ac.singidunum.plugins.PluginContextReader;
import rs.ac.singidunum.plugins.workspaceHandler.composite.CompositeCollection;
import rs.ac.singidunum.plugins.workspaceHandler.composite.WorkspaceTree;
import rs.ac.singidunum.plugins.workspaceHandler.modelPodataka.Document;
import rs.ac.singidunum.plugins.workspaceHandler.modelPodataka.Kolekcija;
import rs.ac.singidunum.plugins.workspaceHandler.modelPodataka.OperacijeModelaWorkspace;
import rs.ac.singidunum.plugins.workspaceHandler.modelPodataka.Workspace;

import javax.swing.*;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;
import java.awt.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicReference;

public class WorkspaceHandler extends JPanel implements Plugin {

    private  ArrayList<Workspace> probaBrisanja = new ArrayList<>();
    private ArrayList<String> otvoreniWorkspaceovi = new ArrayList<>();
    private String  workspaceSepcificationPath;
    private IntegrativnaKomponenta mainWindow;
    private  PluginSpecification pluginSpecification;
    private JTabbedPane multiTab = new JTabbedPane();

    public WorkspaceHandler(PluginSpecification pluginSpecification, IntegrativnaKomponenta mainWindow) {
        this.mainWindow = mainWindow;
        this.pluginSpecification = pluginSpecification;
    }

 public WorkspaceHandler() {
   super();
 }

    public void setWorkspace(DefaultMutableTreeNode workspace){

        JTree workspaceTree  = new JTree(workspace);
        workspaceTree.addTreeSelectionListener(new TreeSelectionListener() {
            public void valueChanged(TreeSelectionEvent e) {
                Object node = workspaceTree.getLastSelectedPathComponent();

                /* if nothing is selected */
                if (node == null) return;

                if(node instanceof Document){
                    Document dokument = (Document) node;
                    if(mainWindow.proveraAktivacije(2)){
                        DocumentHandler dchandler = (DocumentHandler) mainWindow.getActivatedPlugin(2);
                        dchandler.otvorenDokument(dokument.getPutanja());

                    }
                }
            }
        });
        CloseableTabbedPaneWorkspaceHandler closeableTabbedPane = new CloseableTabbedPaneWorkspaceHandler(workspaceTree,this.otvoreniWorkspaceovi,this.probaBrisanja);
        closeableTabbedPane.addTab(multiTab,workspace.getRoot().toString());
//        this.multiTab.add(workspaceTree,workspace.getRoot().toString());

        multiTab.repaint();
        multiTab.revalidate();
        if(!(this.probaBrisanja.contains((Workspace) workspace))){
            this.probaBrisanja.add((Workspace) workspace);
        }
        this.add(multiTab);
        this.repaint();
        this.revalidate();
        this.mainWindow.repaint();
        this.mainWindow.revalidate();
    }
    @Override
    public void activate() {

        LayoutManager constrait  = new BorderLayout();
        this.setLayout(constrait);
        JMenuBar menuBar = new JMenuBar();
        JMenu fileMenu = new JMenu("File");

        JMenuItem open = new JMenuItem("Open");


        JMenuItem buttonDelete = new JMenuItem("Obrisi");
        JMenuItem buttonNewWorkspace = new JMenuItem("New Workspace");
        JMenuItem buttonNewCollection = new JMenuItem("Add Collection");
        JMenuItem buttonNewDocument = new JMenuItem("Add Document");

        fileMenu.add(open);

        fileMenu.add(buttonDelete);
        fileMenu.add(buttonNewWorkspace);
        fileMenu.add(buttonNewCollection);
        fileMenu.add(buttonNewDocument);

        menuBar.add(fileMenu);

        ///////////DELETE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        buttonDelete.addActionListener(e->{
            JTree stablo  = dobaviStablo();
            if(stablo != null){
                OperacijeModelaWorkspace selectovano = (OperacijeModelaWorkspace) stablo.getLastSelectedPathComponent();
                DefaultMutableTreeNode dobavljanjeRoditelja = (DefaultMutableTreeNode) stablo.getLastSelectedPathComponent();
                TreeNode roditelj = dobavljanjeRoditelja.getParent();
                if(roditelj != null){
                    ((OperacijeModelaWorkspace)roditelj).obrisi(selectovano);

                }else{
                    Workspace workspace = (Workspace) selectovano;
                    stablo.removeAll();
                    File temp_file = new File(workspace.getSpecificationPath());
                    System.out.println(temp_file.delete());
                    this.probaBrisanja.remove(workspace);
                    this.otvoreniWorkspaceovi.remove(workspace.getSpecificationPath());
                }

            }
            this.refreshMultitab();

        });
        ///////////DELETE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        buttonNewCollection.addActionListener(e->{

           JTree stablo  = dobaviStablo();
           if(stablo != null){
               OperacijeModelaWorkspace node = (OperacijeModelaWorkspace) stablo.getLastSelectedPathComponent();


               JTextField nazivTextField = new JTextField();
               JLabel nazivLabel = new JLabel("Naziv:");
               JDialog panel = new JDialog();
               panel.setLayout(new GridLayout(2, 2));

               JButton sacuvajButton = new JButton("Sačuvaj");
               sacuvajButton.addActionListener(e2->{
                   node.dodaj(new Kolekcija(nazivTextField.getText()));
                   panel.dispose();
                   this.refreshMultitab();
               });

               panel.add(nazivLabel);
               panel.add(nazivTextField);
               panel.add(new JLabel()); // prazna labela radi boljeg izgleda
               panel.add(sacuvajButton);

               panel.setSize(400,200);
               panel.setVisible(true);
           }

        });
        buttonNewDocument.addActionListener(e->{
            JTree stablo  = dobaviStablo();
            if(stablo != null){
                OperacijeModelaWorkspace node = (OperacijeModelaWorkspace) stablo.getLastSelectedPathComponent();
                TreeNode roditelj = ((DefaultMutableTreeNode)node).getParent();
                TreeNode nuller=null;
                if(roditelj== null){
                    nuller =(TreeNode)node;
                }
                while(roditelj != null){
                    nuller = roditelj;
                    TreeNode temp = ((DefaultMutableTreeNode)roditelj).getParent();
                    roditelj = temp;
                }

                JTextField nazivTextField = new JTextField();
                JLabel nazivLabel = new JLabel("Naziv:");
                JDialog panel = new JDialog();
                panel.setLayout(new GridLayout(2, 2));

                JButton sacuvajButton = new JButton("Sačuvaj");
                TreeNode finalNuller = nuller;
                sacuvajButton.addActionListener(e2->{
                    String naziv = nazivTextField.getText();

                    node.dodaj(new Document(naziv,1L, ((Workspace)((DefaultMutableTreeNode) finalNuller)).getSpecificationPath()));
                    panel.dispose();
                    this.refreshMultitab();
                });

                panel.add(nazivLabel);
                panel.add(nazivTextField);
                panel.add(new JLabel()); // prazna labela radi boljeg izgleda
                panel.add(sacuvajButton);

                panel.setSize(400,200);
                panel.setVisible(true);
            }
        });
        open.addActionListener(e->{
            JFileChooser workspaceChoser = new JFileChooser("src/rs/ac/singidunum/resources/data/Workspace-ovi");
            workspaceChoser.showOpenDialog(mainWindow);
            String []refaktorisanaPutanja;
            if(File.separator.equals("/") ){
                refaktorisanaPutanja = workspaceChoser.getSelectedFile().getPath().split(File.separator);
            }else{
                refaktorisanaPutanja = workspaceChoser.getSelectedFile().getPath().split("\\\\");
            }

            workspaceSepcificationPath = "src"+ File.separator +"rs"+ File.separator +"ac"+ File.separator +"singidunum"+ File.separator +
                    "resources" +File.separator+ "data"+ File.separator+ "Workspace-ovi"+ File.separator + refaktorisanaPutanja[refaktorisanaPutanja.length-1];


            if(this.otvoreniWorkspaceovi.contains(workspaceSepcificationPath)){
                ExceptionHandler.printException("Upozorenje","Workspace je vec otvoren ne mozete otvoriti isti!",mainWindow,new Dimension(300,200) );

            }else{
                WorkspaceTree  workspaceTree= new CompositeCollection();

                Workspace workspace = workspaceTree.execute( workspaceSepcificationPath,null);

                System.out.println(workspaceSepcificationPath);
                this.otvoreniWorkspaceovi.add(workspaceSepcificationPath);
                this.probaBrisanja.add(workspace);
                setWorkspace(workspace);
            }


        });
        buttonNewWorkspace.addActionListener(e->{

            String putanja = "src"+ File.separator +"rs"+ File.separator +"ac"+ File.separator +"singidunum"+ File.separator +
            "resources" +File.separator+ "data"+ File.separator+ "Workspace-ovi"+ File.separator;

            JTextField nazivTextField = new JTextField();
            JLabel nazivLabel = new JLabel("Naziv:");
            JDialog panel = new JDialog();
            panel.setLayout(new GridLayout(2, 2));

            JButton sacuvajButton = new JButton("Sačuvaj");
            sacuvajButton.addActionListener(e2->{
                ///PROVERITI
                String naziv = nazivTextField.getText();
                Workspace noviWorkspace = new Workspace(1L,naziv,putanja+naziv + ".json");
                this.probaBrisanja.add(noviWorkspace);
                this.otvoreniWorkspaceovi.add(noviWorkspace.getSpecificationPath());
                setWorkspace(noviWorkspace);
                panel.dispose();

            });

            panel.add(nazivLabel);
            panel.add(nazivTextField);
            panel.add(new JLabel()); // prazna labela radi boljeg izgleda
            panel.add(sacuvajButton);

            panel.setSize(400,200);
            panel.setVisible(true);

        });

        this.setBackground(Color.WHITE); // Example: Set background color

        Dimension preferredSize = new Dimension(200, 300); // Adjust the dimensions as needed
        this.setPreferredSize(preferredSize);

        this.setBorder(BorderFactory.createSoftBevelBorder(1));

        // Add the new panel to workspacePanel (or directly to the main window)

        this.add(menuBar,BorderLayout.NORTH);
        this.mainWindow.getContentPane().add(this, SpringLayout.WEST);

        this.mainWindow.setLocationRelativeTo(null);
        this.mainWindow.setVisible(true);
        this.mainWindow.revalidate();
        this.mainWindow.repaint();

    }

    @Override
    public void deactivate() {
        if (this != null) {
            // Remove the new panel from the main window
            this.removeAll();
            this.multiTab.removeAll();
            this.probaBrisanja = new ArrayList<>();
            this.otvoreniWorkspaceovi = new ArrayList<>();
            this.mainWindow.getContentPane().remove(this);

            // Revalidate and repaint the main window
            this.mainWindow.revalidate();
            this.mainWindow.repaint();
        }
    }

public void addAktiviraniWorkspace(String workspacePath){
        this.otvoreniWorkspaceovi.add(workspacePath);
}

 @Override
 public PluginSpecification getPluginSpecification() {
  return null;
 }

    @Override
    public Integer getId() {
        return this.pluginSpecification.getId();
    }

    @Override
    public void aktivirajIzKonteksta() {
        this.activate();
        PluginContextReader pluginContextReader = new PluginContextReader();
       ArrayList<String> putanje =  pluginContextReader.readContext(this.pluginSpecification.getPluginContext(), "aktiviraniWorkspaceovi");
        if(putanje == null){

        }else{
            for(String s:putanje){
                WorkspaceTree  workspaceTree= new CompositeCollection();
                this.otvoreniWorkspaceovi.add(s);
                this.setWorkspace(workspaceTree.execute( s,null));
            }
        }

    }

    private void sacuvaj(){
        for(Workspace ws: this.probaBrisanja){
            ws.sacuvaj();
        }
    }
    @Override
    public void sacuvajKontekst() {

        JSONObject obj = new JSONObject();

        // Dodajemo ArrayList u JSON objekat
        JSONArray jsonOtvoreniWorkspacePath = new JSONArray();
        jsonOtvoreniWorkspacePath.addAll(this.otvoreniWorkspaceovi);
        obj.put("aktiviraniWorkspaceovi", jsonOtvoreniWorkspacePath);

        // Pisanje JSON objekta u fajl
        try (FileWriter file = new FileWriter(this.pluginSpecification.getPluginContext())) {
            file.write(obj.toJSONString());
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.sacuvaj();
    }

    private JTree dobaviStablo(){
        JTree stablo = new JTree();
        Component c = multiTab.getSelectedComponent();
        JPanel panel= (JPanel) c;
        Component []components = panel.getComponents();
        JScrollPane pane = (JScrollPane) components[0];
        components = pane.getComponents();
        for(Component comp: components){
//                System.out.println(  comp instanceof JViewport);
//                System.out.println(comp.getClass());
            if(comp instanceof  JViewport){
                JViewport view = (JViewport) comp;
                Component []components2 =view.getComponents();
//                    System.out.println(components2[0] instanceof JTree );
                stablo = (JTree) components2[0];
                return stablo;
            }
        }
        return null;
    }

    private void refreshMultitab(){
        multiTab.removeAll();
        this.remove(multiTab);
        synchronized (this.probaBrisanja){
            if(this.probaBrisanja.isEmpty()){

            }else {
                ArrayList<Workspace> sinhronizacija = new ArrayList<Workspace>(this.probaBrisanja);
//                Iterator<Workspace> iterator = sinhronizacija.iterator();
//                while (iterator.hasNext()) {
//                    Workspace ws = iterator.next();
//                    this.setWorkspace(ws);
//
//                }
                for (Workspace ws : sinhronizacija) {
                    this.setWorkspace(ws);
                }
            }
        }

        multiTab.repaint();
        multiTab.revalidate();
        this.repaint();
        this.revalidate();
    }


}

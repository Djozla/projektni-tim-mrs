package rs.ac.singidunum.plugins.workspaceHandler.modelPodataka;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import rs.ac.singidunum.plugins.DocumentHandler.factory.Page;

import javax.swing.tree.DefaultMutableTreeNode;
import java.io.*;
import java.util.ArrayList;

public class Document extends DefaultMutableTreeNode implements OperacijeModelaWorkspace {

    private Long id;
    private String putanja;
    private String naziv;

    private String owner;
    private ArrayList<String> deljenSa = new ArrayList<>();
    private ArrayList<PageModel> pages = new ArrayList<>();

    public Document(String naziv, String putanja, long id, String owner, ArrayList<PageModel> pages) {
        super(naziv);
        this.naziv = naziv;
        this.putanja = putanja;
        this.id = id;
        this.owner = owner;
        this.pages = pages;
    }

    public Document(String naziv, long id, String owner) {
        super(naziv);
        this.naziv = naziv;
        this.id = id;
        this.owner = owner;
    }

    public Document(String naziv, Long id) {
        super(naziv);
        this.naziv = naziv;
        this.id = id;
    }

    public Document(String naziv) {
        super(naziv);
        this.putanja = putanja;
    }

    public String getPutanja() {
        return putanja;
    }

    public ArrayList<String> getDeljenSa() {
        return deljenSa;
    }

    public boolean daLiJeDeljen() {
        if (this.deljenSa.size() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public static Document ucitajDokument(String putanjaDokumenta) {
        JSONParser parser = new JSONParser();
        ArrayList<PageModel> pages = new ArrayList<>();
        ArrayList<String> deljenSa = new ArrayList<>();
        try {
            FileReader reader = new FileReader(putanjaDokumenta);
            Object obj = parser.parse(reader);
            JSONObject jsonObject = (JSONObject) obj;

            long id = (Long) jsonObject.get("id");
            String naziv = (String) jsonObject.get("naziv");
            String owner = (String) jsonObject.get("owner");

            JSONObject pagesKljuc = (JSONObject) jsonObject.get("pages");
            for(Object pageKey:pagesKljuc.keySet()){
                String pageName = (String) pageKey;
                JSONObject page = (JSONObject) pagesKljuc.get(pageName);
                String thumbnail = (String) page.get("thumbnail");
                pages.add(new PageModel(pageName, thumbnail));
            }

            return new Document(naziv, putanjaDokumenta, id, owner, pages);

        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }

    }

    public String sacuvaj() {
        JSONObject object = new JSONObject();
        JSONArray deljenSa = new JSONArray();
        ArrayList<JSONObject> pages = new ArrayList<>();
        JSONArray pagesCuvanje = new JSONArray();
        deljenSa.addAll(this.deljenSa);

        object.put("id", this.id);
        object.put("naziv", this.naziv);
        object.put("owner", this.owner);
        object.put("podeljen", this.deljenSa);

        if (this.pages.isEmpty()) {
            object.put("pages", new JSONObject());
        } else {
            JSONObject proba = new JSONObject();
            for (PageModel p : this.pages) {
                proba.put(p.getNaziv(),p.sacuvaj());
            }
            object.put("pages", proba);

        }

        String docPath = "src" + File.separator + "rs" + File.separator + "ac" + File.separator + "singidunum" + File.separator +
                "resources" + File.separator + "data" + File.separator + "Documents" + File.separator + this.naziv + ".json";
        try (FileWriter file = new FileWriter(docPath)) {
            file.write(object.toJSONString());
        } catch (IOException e) {
            e.printStackTrace();

        }
        String zaVratiti;
        if(File.separator.equals("/")){
            zaVratiti = ".." + File.separator + "Documents" + File.separator + this.naziv + ".json";

        }else{
            zaVratiti = "../" + "Documents/" + this.naziv + ".json";
        }


        return zaVratiti;
    }

    @Override
    public void dodaj(OperacijeModelaWorkspace model) {

    }

    @Override
    public void obrisi(OperacijeModelaWorkspace model) {

    }
}

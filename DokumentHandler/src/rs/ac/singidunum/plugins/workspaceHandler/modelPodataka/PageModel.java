package rs.ac.singidunum.plugins.workspaceHandler.modelPodataka;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import rs.ac.singidunum.plugins.DocumentHandler.factory.PageStrip;

import java.util.ArrayList;

public class PageModel {
    String naziv;
    private String thumbnail;
    private ArrayList<String> slots = new ArrayList<>();

    public PageModel(String naziv, String thumbnail){
        this.naziv = naziv;
        this.thumbnail = thumbnail;
    }
    public PageModel(String naziv,String thumbnail,ArrayList<String> slots){
        this.naziv = naziv;
        this.thumbnail = thumbnail;
        this.slots=slots;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public ArrayList<String> getSlots() {
        return slots;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public void setSlots(ArrayList<String> slots) {
        this.slots = slots;
    }

    public String getNaziv() {
        return naziv;
    }

    public static PageModel ucitajPage(JSONObject pages){

        for (Object pageObj : pages.keySet()) {
            String pageName = (String) pageObj;
            JSONObject page = (JSONObject) pages.get(pageName);

            // Extract values for the current page
            String thumbnail = (String) page.get("thumbnail");
            System.out.println(pageName);
            return new PageModel("asd", thumbnail);
        }
        return null;
    }

    public JSONObject sacuvaj(){
        JSONObject object = new JSONObject();


        object.put("thumbnail", this.thumbnail);
        object.put("slotovi", new JSONObject());

        return object;
    }
}

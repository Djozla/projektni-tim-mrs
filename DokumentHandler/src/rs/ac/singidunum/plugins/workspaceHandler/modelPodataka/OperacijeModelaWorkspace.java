package rs.ac.singidunum.plugins.workspaceHandler.modelPodataka;

public interface OperacijeModelaWorkspace {
    public void dodaj(OperacijeModelaWorkspace model);
    public void obrisi(OperacijeModelaWorkspace model);

}

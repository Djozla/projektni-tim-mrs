package rs.ac.singidunum.plugins.workspaceHandler.modelPodataka;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import javax.swing.tree.DefaultMutableTreeNode;
import java.util.ArrayList;

public class Kolekcija extends DefaultMutableTreeNode implements OperacijeModelaWorkspace{

    private ArrayList<Kolekcija> kolekcije = new ArrayList<>();
    private ArrayList<Document> documents = new ArrayList<>();
    private String naziv;

    public Kolekcija(String naziv){
        super(naziv);
        this.naziv = naziv;
    }
    public Kolekcija(){
        super();
    }
    public ArrayList<Kolekcija> getCollections() {
        return kolekcije;
    }

    public void setCollections(ArrayList<Kolekcija> kolekcijas) {
        for(Kolekcija kolekcija : kolekcijas){
            this.kolekcije.add(kolekcija);
            this.add(kolekcija);
        }
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public ArrayList<Document> getDokumenti() {
        return documents;
    }

    public void setDokumenti(ArrayList<Document> dokumenti) {
        for(Document document: dokumenti){
            this.documents.add(document);
            this.add(document);
        }
    }

    public void addCollection(Kolekcija kolekcija){
        if(kolekcija!=null){
            this.kolekcije.add(kolekcija);
            this.add(kolekcija);
        }

    }

    public void addDocument(Document document){
        if(document != null){
            this.documents.add(document);
            this.add(document);
        }

    }

    public void proveriBrisanje(){

    }

    @Override
    public void dodaj(OperacijeModelaWorkspace model) {
        if(model instanceof Kolekcija){
            this.add((DefaultMutableTreeNode) model);
            kolekcije.add((Kolekcija) model);
        } else if (model instanceof Document) {
            this.add((DefaultMutableTreeNode) model);
            this.documents.add((Document) model);

        }
    }
    @Override
    public void obrisi(OperacijeModelaWorkspace model) {
        if(model instanceof Kolekcija){
            this.remove((Kolekcija) model);
            kolekcije.remove((Kolekcija) model);
        } else if (model instanceof Document) {
            if(((Document) model).daLiJeDeljen()){
                //alp ke trie
            }else{
                this.remove((Document) model);
                this.documents.remove((Document) model);
            }
        }
    }
    public JSONObject sacuvaj(){
        JSONObject object = new JSONObject();
        ArrayList<JSONObject> kolekcije = new ArrayList<>();
        JSONArray kolekcijeCuvanje = new JSONArray();
        ArrayList<String> dokumenti = new ArrayList<>();
        JSONArray dokumentiCuvanje = new JSONArray();

        object.put("naziv", this.naziv);

        if(this.kolekcije.isEmpty()){
            JSONArray array = new JSONArray();
            array.addAll(new ArrayList<>());
            object.put("collections", array);
        }else{
            for(Kolekcija k: this.kolekcije){
                kolekcije.add(k.sacuvaj());
            }
            kolekcijeCuvanje.addAll(kolekcije);
            object.put("collections", kolekcijeCuvanje);
        }

        if(this.documents.isEmpty()){
            JSONArray array = new JSONArray();
            array.addAll(new ArrayList<>());
            object.put("dokumenti",array);
        }else{
            for(Document d: this.documents){
                dokumenti.add(d.sacuvaj());
            }
            dokumentiCuvanje.addAll(dokumenti);
            object.put("dokumenti",dokumentiCuvanje);
        }


        return object;
    }
}

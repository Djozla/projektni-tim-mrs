package rs.ac.singidunum.plugins.workspaceHandler.modelPodataka;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import javax.swing.tree.DefaultMutableTreeNode;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class Workspace extends DefaultMutableTreeNode implements OperacijeModelaWorkspace{

    private Long id;
    private String naziv;
    private String specificationPath;
    private ArrayList<Kolekcija> kolekcije = new ArrayList<>();
    private ArrayList<Document> documents = new ArrayList<>();

    public Workspace(Long id, String naziv,String specificationPath){
        super(naziv);
        this.id = id;
        this.naziv = naziv;
        this.specificationPath = specificationPath;
    }
    public Workspace(Long id, String naziv){
        super(naziv);
        this.id = id;
        this.naziv = naziv;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public ArrayList<Kolekcija> getKolekcije() {
        return kolekcije;
    }

    public void setKolekcije(ArrayList<Kolekcija> kolekcije) {
        this.kolekcije = kolekcije;
    }

    public ArrayList<Document> getDocuments() {
        return documents;
    }

    public void setDocuments(ArrayList<Document> documents) {
        this.documents = documents;
    }

    public String getSpecificationPath() {
        return specificationPath;
    }

    public void addCollection(Kolekcija kolekcija){
        if(kolekcija!=null){
            this.kolekcije.add(kolekcija);
            this.add(kolekcija);
        }

    }

    public void addDocument(Document document){
        if(document != null){
            this.documents.add(document);
            this.add(document);
        }

    }


    @Override
    public void dodaj(OperacijeModelaWorkspace model) {
        if(model instanceof Kolekcija){
            this.add((DefaultMutableTreeNode) model);
            kolekcije.add((Kolekcija) model);
        } else if (model instanceof Document) {
            this.add((DefaultMutableTreeNode) model);
            this.documents.add((Document) model);

        }
    }

    @Override
    public void obrisi(OperacijeModelaWorkspace model) {
        if(model instanceof Kolekcija){
            this.remove((DefaultMutableTreeNode) model);
            kolekcije.remove((Kolekcija) model);
        } else if (model instanceof Document) {
            if(((Document) model).daLiJeDeljen()){
            }else{
                File temp_file = new File(((Document) model).getPutanja());
                this.remove((DefaultMutableTreeNode) model);
                this.documents.remove((Document) model);
                temp_file.deleteOnExit();
            }
        }
    }
    public void sacuvaj(){
        JSONObject object = new JSONObject();
        ArrayList<JSONObject> kolekcije =new ArrayList<>();
        JSONArray kolekcijeCuvanje = new JSONArray();
        ArrayList<String> dokumenti =new ArrayList<>();
        JSONArray dokumentiCuvanje = new JSONArray();

        object.put("id", this.id);
        object.put("naziv", this.naziv);

        if(this.kolekcije.isEmpty()){
            JSONArray array = new JSONArray();
            array.addAll(new ArrayList<>());
            object.put("collections", array);
        }else{
            for(Kolekcija k : this.kolekcije){
                kolekcije.add(k.sacuvaj());
            }
            kolekcijeCuvanje.addAll(kolekcije);
            object.put("collections", kolekcijeCuvanje);
        }

        if(this.documents.isEmpty()){
            JSONArray array = new JSONArray();
            array.addAll(new ArrayList<>());
            object.put("dokumenti", array);
        }else{
            for(Document d : this.documents){
                dokumenti.add(d.sacuvaj());
            }
            dokumentiCuvanje.addAll(dokumenti);
            object.put("dokumenti", dokumentiCuvanje);
        }

        try (FileWriter file = new FileWriter(this.specificationPath)) {
            file.write(object.toJSONString());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    public static void ukloniWSTab(ArrayList<Workspace> workspaces,String putanja ){
        for(Workspace ws: workspaces){
            if(ws.getSpecificationPath().equals(putanja)){
                workspaces.remove(ws);
            }
        }
    }
}

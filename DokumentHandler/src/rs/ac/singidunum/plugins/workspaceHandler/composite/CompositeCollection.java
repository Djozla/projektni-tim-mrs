package rs.ac.singidunum.plugins.workspaceHandler.composite;

import rs.ac.singidunum.plugins.workspaceHandler.Iterator.ConcreteIterator;
import rs.ac.singidunum.plugins.workspaceHandler.Iterator.Iterator;
import rs.ac.singidunum.plugins.workspaceHandler.modelPodataka.Workspace;

import javax.swing.tree.DefaultMutableTreeNode;
import java.util.ArrayList;

public class CompositeCollection implements WorkspaceTree{

    private Long id;
    private String naziv;

    private ArrayList<String> putanjeSpecifikacijeKolekcija = new ArrayList<>();
    private ArrayList<String> putanjeSpecifikacijeDokumenata = new ArrayList<>();




    public CompositeCollection(){}

    public void addPutanjuKolekcije(String putanja) {
        this.putanjeSpecifikacijeKolekcija.add(putanja);
    }


    public void addPutanjuDokumenta(String putanja) {
        this.putanjeSpecifikacijeDokumenata.add(putanja);
    }

    public void setID(Long id) {
        this.id= id;
    }

    @Override
    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    @Override
    public Workspace execute(String filePath, DefaultMutableTreeNode parent) {

        Iterator iterator = new ConcreteIterator();


//        WorkspaceTree kolekcije = new CompositeCollection();
//        WorkspaceTree dokumenti = new DocumentDeskriptor();
//
//        DefaultMutableTreeNode workspaceRoot = new DefaultMutableTreeNode(this.naziv);
//        for(String kolekcijaPutanja: this.putanjeSpecifikacijeKolekcija){
//
//            workspaceRoot.add(kolekcije.execute(kolekcijaPutanja,workspaceRoot));
//            //Ovde cemo konstruisati objekat tipa kolekcije
//        }
//        for(String dokument: this.putanjeSpecifikacijeDokumenata){
//
//            workspaceRoot.add(dokumenti.execute(dokument,workspaceRoot));
//        }

        return iterator.citaj(filePath);
    }
}

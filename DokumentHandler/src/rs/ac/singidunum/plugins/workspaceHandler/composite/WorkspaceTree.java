package rs.ac.singidunum.plugins.workspaceHandler.composite;

import rs.ac.singidunum.plugins.workspaceHandler.modelPodataka.Workspace;

import javax.swing.tree.DefaultMutableTreeNode;

public interface WorkspaceTree {
    public Workspace execute(String filePath, DefaultMutableTreeNode parent);
    public void addPutanjuKolekcije(String putanja);
    public void addPutanjuDokumenta(String putanja);
    public void setID(Long id);
    public void setNaziv(String naziv);
}

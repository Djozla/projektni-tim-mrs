package rs.ac.singidunum.plugins;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class PluginContextReader {

    public PluginContextReader(){};
    private  ArrayList<String>putanje = new ArrayList<>();
    public  ArrayList<String> readContext(String contextPath,String JSONkey){
        JSONParser jsonParser = new JSONParser();

        try (FileReader reader = new FileReader(contextPath)) {

            // Parse the JSON file
            Object obj = jsonParser.parse(reader);

            // Cast the parsed object to JSONObject
            JSONObject jsonObject = (JSONObject) obj;

            // Get the "aktiviraniWorkspaceovi" array
            JSONArray activatedWorkspacesArray = (JSONArray) jsonObject.get(JSONkey);

            // Iterate over the array and add each workspace path to the list
            for (Object workspaceObj : activatedWorkspacesArray) {
                String workspace = (String) workspaceObj;
                putanje.add(workspace);
            }

        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
        return putanje;
    }
}

package rs.ac.singidunum.plugins.DocumentHandler.factory;

import rs.ac.singidunum.plugins.DocumentHandler.model.DocumentH;

public interface DocumentProduct {

    public DocumentProduct create(DocumentH document);
    public void update();
    public void delete();
}

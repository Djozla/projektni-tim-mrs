package rs.ac.singidunum.plugins.DocumentHandler.factory;

import rs.ac.singidunum.plugins.DocumentHandler.model.DocumentH;

public interface Creator {
    public DocumentProduct create(DocumentH document);

}

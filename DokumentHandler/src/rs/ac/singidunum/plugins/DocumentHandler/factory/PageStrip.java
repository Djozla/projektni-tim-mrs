package rs.ac.singidunum.plugins.DocumentHandler.factory;

import rs.ac.singidunum.plugins.DocumentHandler.model.DocumentH;
import javax.swing.*;
import javax.swing.border.BevelBorder;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collections;


public class PageStrip extends JPanel implements DocumentProduct{

    ArrayList<String >thumbnailPutanje;

    public PageStrip(){
        this.setLayout(new GridLayout(0,1) );
        this.setBackground(Color.GRAY);
    }

    private void addThumbnail(){

    }

    private static ImageIcon addTextToIcon(ImageIcon originalIcon, String text) {
        // Convert the ImageIcon to BufferedImage
        BufferedImage image = new BufferedImage(
                originalIcon.getIconWidth(),
                originalIcon.getIconHeight(),
                BufferedImage.TYPE_INT_ARGB
        );
        Graphics2D g2d = image.createGraphics();
        originalIcon.paintIcon(null, g2d, 0, 0);
        g2d.dispose();

        // Add text to the BufferedImage
        Graphics2D g2dText = image.createGraphics();
        g2dText.setColor(Color.BLACK); // Set the text color
        g2dText.setFont(new Font("Arial", Font.BOLD, 24)); // Set the font

        // Calculate text position (centered in this example)
        int x = (image.getWidth() - g2dText.getFontMetrics().stringWidth(text)) / 2;
        int y = image.getHeight() / 2;

        // Draw the text onto the image
        g2dText.drawString(text, x, y);
        g2dText.dispose();

        // Create a new ImageIcon from the modified BufferedImage
        return new ImageIcon(image);
    }




    @Override
    public DocumentProduct create(DocumentH document) {

        this.setLayout(new GridLayout(0,1));
        thumbnailPutanje = document.getThumbnailPutanje();


        int i=0;
        for(String putanja: thumbnailPutanje){

//           SLIKA
            BufferedImage myPicture ;
            JLabel picLabel = new JLabel();
            ImageIcon originalImage = new ImageIcon(putanja);
            ImageIcon scaledImage = new ImageIcon(originalImage.getImage().getScaledInstance(originalImage.getIconWidth() / 10,originalImage.getIconHeight() / 8, Image.SCALE_SMOOTH));
            ImageIcon modifiedIcon = addTextToIcon(scaledImage, document.getPageName().get(i));

            modifiedIcon.setDescription(putanja);


            JLabel pageNumLabel = new JLabel(document.getPageName().get(i));


            picLabel.setLayout(new GridBagLayout());
//           picLabel = new JLabel(scaledImage);
            picLabel = new JLabel(modifiedIcon);
            picLabel.add(pageNumLabel);
            picLabel.setBorder(new BevelBorder(2));

//           ACTION LISTENER
            int finalI = i;
            picLabel.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {


                    if(e.getSource() instanceof JLabel){
                        JLabel clickedLabel = (JLabel) e.getSource();
//                       System.out.println("clickedLabel: " + clickedLabel);
                        ImageIcon clickedIcon = (ImageIcon) clickedLabel.getIcon();
                        System.out.println("clickedIcon: "+ clickedIcon);
                        System.out.println("Clicked on: " + document.getPageName().get(finalI));
                    }
                }
            });
            picLabel.setBorder(new BevelBorder(2));


            this.add(picLabel);


            i++;
        }
        return this;
    }



    @Override
    public void update() {

    }

    @Override
    public void delete() {

    }
}

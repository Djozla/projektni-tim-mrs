package rs.ac.singidunum.plugins.DocumentHandler;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import rs.ac.singidunum.core.ui.pluginManager.plugin.Plugin;
import rs.ac.singidunum.core.ui.pluginManager.plugin.PluginSpecification;
import rs.ac.singidunum.plugins.DocumentHandler.factory.DocumentProduct;
import rs.ac.singidunum.plugins.DocumentHandler.factory.PageStrip;
import rs.ac.singidunum.plugins.DocumentHandler.model.DocumentH;
import rs.ac.singidunum.plugins.PluginContextReader;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class DocumentHandler extends JPanel implements Plugin {

    private JFrame mainWindow;
    private ArrayList<String> otvoreniDokumenti = new ArrayList<>();
    private PluginSpecification pluginSpecification;
    private JToolBar toolBar = new JToolBar();
    private JTabbedPane multiTab = new JTabbedPane();

    public DocumentHandler(PluginSpecification pluginSpecification, JFrame mainWindow) {
        this.mainWindow = mainWindow;
        this.pluginSpecification = pluginSpecification;

    }


    private JToolBar createToolBar() {
        // Create a toolbar
        JToolBar toolbar = new JToolBar();
        JButton newPageButton = new JButton("New Page");
        JButton deletePageButton = new JButton("Delete Page");
        newPageButton.setCursor(new Cursor(Cursor.HAND_CURSOR));
        deletePageButton.setCursor(new Cursor(Cursor.HAND_CURSOR));
        newPageButton.setToolTipText("New Page");
        deletePageButton.setToolTipText("New Page");

        // Add ActionListener to the "New Page" button
        newPageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // Call a method to handle the creation of a new page
//                createNewPage(otvoreniDokumenti.toString());
                System.out.println(otvoreniDokumenti);
            }
        });

        toolbar.add(newPageButton);
        toolbar.add(deletePageButton);

        return toolbar;
    }

    public void otvorenDokument(String dokumentPath){


        if(this.otvoreniDokumenti.contains(dokumentPath)){
            JDialog dijalog = new JDialog(mainWindow,"Upozorenje");
            dijalog.setLayout(new BorderLayout());
            dijalog.setSize(300,200);
            JButton buttonOK = new JButton("Ok");
            buttonOK.addActionListener(o->{
                dijalog.setVisible(false);
            });
            dijalog.add(new JLabel("Dokument je vec otvoren ne mozete otvoriti isti!"),BorderLayout.CENTER);
            dijalog.add(buttonOK,BorderLayout.SOUTH);
            dijalog.setVisible(true);
            dijalog.setVisible(true);
        }else{
            DocumentProduct pageStrip = new PageStrip();
            DocumentH document = new DocumentH(dokumentPath);
            DocumentProduct pageStripPanel = pageStrip.create(document);


            JPanel page = new JPanel(new BorderLayout());
            JScrollPane scrollPane = new JScrollPane((Component) pageStripPanel, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
            JSplitPane split = new JSplitPane( JSplitPane.HORIZONTAL_SPLIT, scrollPane, page );

            split.setContinuousLayout( true );
            page.setBackground(Color.darkGray);


            JPanel zamenskiPanel = new JPanel(new GridBagLayout());


            this.otvoreniDokumenti.add(dokumentPath);
            this.addComponent(zamenskiPanel,  split,0, 2, 1, 1, 0.2, 1.0, GridBagConstraints.BOTH);
            this.addComponent(zamenskiPanel, page, 1, 2, 1, 1, 0.8, 1.0, GridBagConstraints.BOTH);
            CloseableTabbedPaneDocumentHandler zatvarajuciTabovi = new CloseableTabbedPaneDocumentHandler(zamenskiPanel,this.otvoreniDokumenti);
            zatvarajuciTabovi.addTab(multiTab,document.getNaziv());
            this.add(multiTab,BorderLayout.CENTER);
            this.repaint();
            this.revalidate();
        }

    }

    @Override
    public void activate() {

        this.setLayout(new BorderLayout());

        this.toolBar = createToolBar();
        this.add(this.toolBar,BorderLayout.NORTH);
        this.setBorder(BorderFactory.createSoftBevelBorder(1));

        this.mainWindow.getContentPane().add(this);
        this.mainWindow.setLocationRelativeTo(null);
        this.mainWindow.setVisible(true);
        this.mainWindow.revalidate();
        this.mainWindow.repaint();

    }


    @Override
    public void deactivate() {
        if (this != null) {

            this.removeAll();
            multiTab.removeAll();
            otvoreniDokumenti = new ArrayList<>();
            // Remove the new panel from the main window
            this.mainWindow.getContentPane().remove(this);

            // Revalidate and repaint the main window
            this.mainWindow.revalidate();
            this.mainWindow.repaint();

        }
    }

    @Override
    public Integer getId() {
        return this.pluginSpecification.getId();
    }

    @Override
    public void aktivirajIzKonteksta() {
        this.activate();
        PluginContextReader pluginContextReader = new PluginContextReader();
        ArrayList<String> dokumenti = pluginContextReader.readContext(this.pluginSpecification.getPluginContext(),"aktiviraniDocumenti");
        if(dokumenti == null){

        }else{
            for(String s: dokumenti){
                this.otvorenDokument(s);
                this.mainWindow.revalidate();
                this.mainWindow.repaint();
            }
        }

    }

    @Override
    public void sacuvajKontekst() {

        System.out.println("DOCUMENT CONTEXT CUVANJE");
        JSONObject obj = new JSONObject();

        // Dodajemo ArrayList u JSON objekat
        JSONArray jsonOtvoreniDokumentPath = new JSONArray();
        jsonOtvoreniDokumentPath.addAll(this.otvoreniDokumenti);
        obj.put("aktiviraniDocumenti", jsonOtvoreniDokumentPath);


        // Pisanje JSON objekta u fajl
        try (FileWriter file = new FileWriter(this.pluginSpecification.getPluginContext())) {
            file.write(obj.toJSONString());
            System.out.println("DOCUMENT CONTEXT USPESNO SACUVAN");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    @Override
    public PluginSpecification getPluginSpecification() {
        return this.pluginSpecification;
    }

    public void addComponent(JPanel panel, JPanel button, int gridx, int gridy, int gridwidth, int gridheight, double weightx, double weighty, int fill) {
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = gridx;
        gbc.gridy = gridy;
        gbc.gridwidth = gridwidth;
        gbc.gridheight = gridheight;
        gbc.weightx = weightx;
        gbc.weighty = weighty;
        gbc.fill = fill;

        panel.add(button, gbc);

    }
    public void addComponent(JPanel panel, JMenuBar button, int gridx, int gridy, int gridwidth, int gridheight, double weightx, double weighty, int fill) {
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = gridx;
        gbc.gridy = gridy;
        gbc.gridwidth = gridwidth;
        gbc.gridheight = gridheight;
        gbc.weightx = weightx;
        gbc.weighty = weighty;
        gbc.fill = fill;

        panel.add(button, gbc);

    }
    public void addComponent(JPanel panel, JSplitPane button, int gridx, int gridy, int gridwidth, int gridheight, double weightx, double weighty, int fill) {
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = gridx;
        gbc.gridy = gridy;
        gbc.gridwidth = gridwidth;
        gbc.gridheight = gridheight;
        gbc.weightx = weightx;
        gbc.weighty = weighty;
        gbc.fill = fill;

        panel.add(button, gbc);

    }
}

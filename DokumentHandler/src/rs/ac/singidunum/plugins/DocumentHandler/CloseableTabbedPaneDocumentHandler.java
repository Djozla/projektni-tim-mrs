package rs.ac.singidunum.plugins.DocumentHandler;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.io.File;
import java.util.ArrayList;

public class CloseableTabbedPaneDocumentHandler {
    private JPanel zamenskiPanel;
    private ArrayList<String> putanje = new ArrayList<>();

    public CloseableTabbedPaneDocumentHandler(JPanel zamenskiPanel, ArrayList<String> putanje){
        this.zamenskiPanel = zamenskiPanel;
        this.putanje = putanje;

    }

    public void addTab(JTabbedPane tabbedPane, String title) {

        // Create a close button
        JButton closeButton = new JButton("X");


        // Create a panel for the tab title with a close button
        JPanel tabTitlePanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
        tabTitlePanel.add(new JLabel(title));
        tabTitlePanel.add(closeButton);

        // Create a panel for the tab content (tree)
        JPanel panel = new JPanel(new BorderLayout());
        panel.add(new JScrollPane(zamenskiPanel), BorderLayout.CENTER);
        closeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int tabIndex = tabbedPane.indexOfComponent(panel);
                if (tabIndex != -1) {
                    String provera2 = "";
                    for(String s: putanje){
                        String [] provera;
                        if(File.separator.equals("/")){
                            provera = s.split(File.separator);
                        }else{
                            provera = s.split("\\\\");
                        }
                        provera2 = provera[provera.length-1];
                        if(provera[provera.length-1].equals(title+".json")){
                            putanje.remove(s);
                            tabbedPane.remove(tabIndex);
                        }

                    }
//                    System.out.println(title);
//                    System.out.println(provera2);

                }
            }
        });

        // Add the tab to the JTabbedPane with the custom title panel
        tabbedPane.addTab(null, panel);
        tabbedPane.setTabComponentAt(tabbedPane.getTabCount() - 1, tabTitlePanel);

    }
}

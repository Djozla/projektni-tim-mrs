package rs.ac.singidunum.plugins.DocumentHandler.model;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import rs.ac.singidunum.resources.data.formatiranjePutanja;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class DocumentH {

    private ArrayList<String> thumbnailPutanje = new ArrayList<>();
    private ArrayList<String> pageName = new ArrayList<>();
    private String naziv;

    public DocumentH(String documentPath){
        ucitajSpecifikaciju(documentPath);
    }






    private void ucitajSpecifikaciju(String specificationPath){
        try (FileReader reader = new FileReader(specificationPath)) {
            // Parse JSON string
            JSONParser parser = new JSONParser();
            try {
                JSONObject jsonObject = (JSONObject) parser.parse(reader);
                naziv = (String)jsonObject.get("naziv");

                // Get the "pages" object
                JSONObject pagesObject = (JSONObject) jsonObject.get("pages");

                // Convert keys to a list and reverse it
                List<String> pageKeys = new ArrayList<>(pagesObject.keySet());
                Collections.reverse(pageKeys);

                // Iterate through each page in reversed order
                for (String pageName : pageKeys) {
                    JSONObject pageObject = (JSONObject) pagesObject.get(pageName);
                    System.out.println("pageName: " + pageName);
                    this.pageName.add(pageName);

                    String putanja = (String) pageObject.get("thumbnail");
                    this.thumbnailPutanje.add(formatiranjePutanja.napraviPutanju(specificationPath, putanja));

                    // Access other properties as needed
                }

            } catch (ParseException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }




    public String getNaziv() {
        return naziv;
    }

    public ArrayList<String> getThumbnailPutanje() {
        return thumbnailPutanje;
    }

    public ArrayList<String> getPageName() {
        return pageName;
    }
}

package rs.ac.singidunum.plugins.ElementHandler;

import rs.ac.singidunum.core.ui.pluginManager.plugin.Plugin;
import rs.ac.singidunum.core.ui.pluginManager.plugin.PluginSpecification;

import javax.swing.*;
import java.awt.*;

public class ElementHandler extends JPanel implements Plugin {
    private JFrame mainWindow;
    private PluginSpecification pluginSpecification;

    public ElementHandler(PluginSpecification pluginSpecification, JFrame mainWindow) {
        this.mainWindow = mainWindow;
        this.pluginSpecification = pluginSpecification;
    }
    @Override
    public void activate() {
        JPanel newPanel = new JPanel();
        this.setBackground(Color.WHITE); // Example: Set background color

        Dimension preferredSize = new Dimension(200, 300); // Adjust the dimensions as needed
        this.setPreferredSize(preferredSize);
        this.setToolTipText("IN PROGRESS");
        this.setBorder(BorderFactory.createSoftBevelBorder(1));

        // Add the new panel to workspacePanel (or directly to the main window)
        this.mainWindow.getContentPane().add(this, BorderLayout.SOUTH);


        this.mainWindow.setLocationRelativeTo(null);
        this.mainWindow.setVisible(true);
        this.mainWindow.revalidate();
        this.mainWindow.repaint();

    }

    @Override
    public void deactivate() {
        if (this != null) {
            // Remove the new panel from the main window
            this.mainWindow.getContentPane().remove(this);

            // Revalidate and repaint the main window
            this.mainWindow.revalidate();
            this.mainWindow.repaint();
        }
    }


    @Override
    public PluginSpecification getPluginSpecification() {
        return null;
    }
    @Override
    public Integer getId() {
        return this.pluginSpecification.getId();
    }

    @Override
    public void aktivirajIzKonteksta() {

    }

    @Override
    public void sacuvajKontekst() {

    }
}



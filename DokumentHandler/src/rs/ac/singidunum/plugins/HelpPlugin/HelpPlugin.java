package rs.ac.singidunum.plugins.HelpPlugin;

import rs.ac.singidunum.core.ui.IntegrativnaKomponenta;
import rs.ac.singidunum.core.ui.pluginManager.plugin.Plugin;
import rs.ac.singidunum.core.ui.pluginManager.plugin.PluginSpecification;

import javax.swing.*;
import java.awt.*;

public class HelpPlugin implements Plugin {

    private JMenu helpMenu;
    private IntegrativnaKomponenta parent;
    private PluginSpecification pluginSpecification;
    public HelpPlugin(PluginSpecification pluginSpecification, IntegrativnaKomponenta parent){
        this.parent = parent;
        this.pluginSpecification = pluginSpecification;
        this.helpMenu = new JMenu("Help");

        JMenuItem helpPlugin = new JMenuItem("Help Plugin");
        helpPlugin.addActionListener(e -> {
            JDialog dijalog = new JDialog(parent,"Help plugin");
            dijalog.setLayout(new BorderLayout());
            dijalog.setSize(450,200);
            JButton buttonOK = new JButton("Ok");
            buttonOK.addActionListener(o->{
                dijalog.setVisible(false);
            });
            dijalog.add(new JLabel("Za vise informacija posetiti : https://gitlab.com/Djozla/projektni-tim-mrs"),BorderLayout.CENTER);
            dijalog.add(buttonOK,BorderLayout.SOUTH);
            dijalog.setVisible(true);
        });

        this.helpMenu.add(helpPlugin);
    }
    @Override
    public void activate() {

        parent.addMenuItem(helpMenu);
    }

    @Override
    public void deactivate() {
        parent.removeMenuItem(this.helpMenu);
    }

    @Override
    public Integer getId() {
        return this.pluginSpecification.getId();
    }

    @Override
    public void aktivirajIzKonteksta() {
        this.activate();
    }

    @Override
    public void sacuvajKontekst() {

    }

    @Override
    public PluginSpecification getPluginSpecification() {
        return this.pluginSpecification;
    }
}

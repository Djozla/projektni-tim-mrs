package rs.ac.singidunum;


import rs.ac.singidunum.config.Configuration;
import rs.ac.singidunum.core.ui.IntegrativnaKomponenta;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.*;
import rs.ac.singidunum.core.ui.pluginManager.plugin.PluginRegistry;
import rs.ac.singidunum.plugins.workspaceHandler.WorkspaceHandler;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;


public class Main {

    public static final String macOS = "macos";
    public static final String Windows = "windows";



    public static void main(String[] args) {

        JSONObject object = new JSONObject();
        try {
            JSONParser jsonParser = new JSONParser();


            JSONObject configurationJSON = (JSONObject) jsonParser.parse(new FileReader(new File("src"+ File.separator +"rs"+ File.separator +"ac"+ File.separator +"singidunum"+ File.separator +"resources"+ File.separator +"data"+ File.separator +"configuration.json")));


            Configuration config = IntegrativnaKomponenta.loadConfiguration(configurationJSON);

            if (config != null) {
                IntegrativnaKomponenta mainWindow = new IntegrativnaKomponenta(config);
                PluginRegistry pluginRegistry = new PluginRegistry(config.getPluginsPath(),mainWindow);



                mainWindow.setPluginRegistry(pluginRegistry);
                mainWindow.init();
            } else {
                System.out.println("Failed to load configuration.");
            }
        } catch (IOException | ParseException e) {
            e.printStackTrace();
            System.out.println("An error occurred while reading or parsing the JSON file.");
        }


    }
}
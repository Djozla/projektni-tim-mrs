package rs.ac.singidunum.core.ui.pluginManager.plugin;

public class Author {
    private String name;
    private String email;
    private String web_page;

    public Author(String name, String email, String web_page) {
        this.name = name;
        this.email = email;
        this.web_page = web_page;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWeb_page() {
        return web_page;
    }

    public void setWeb_page(String web_page) {
        this.web_page = web_page;
    }
}

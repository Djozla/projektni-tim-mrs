package rs.ac.singidunum.core.ui.pluginManager.plugin;

import java.io.*;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import rs.ac.singidunum.core.ui.IntegrativnaKomponenta;
import rs.ac.singidunum.plugins.DocumentHandler.DocumentHandler;
import rs.ac.singidunum.plugins.ElementHandler.ElementHandler;
import rs.ac.singidunum.plugins.HelpPlugin.HelpPlugin;
import rs.ac.singidunum.plugins.workspaceHandler.WorkspaceHandler;


public class PluginRegistry {
    private String path; // folder
    private IntegrativnaKomponenta parent;
    private ArrayList<Plugin> plugins = new ArrayList<>();

    ArrayList<PluginSpecification> pluginSpecifikacije = new ArrayList<>();

    public PluginRegistry(String path, IntegrativnaKomponenta iface) {
            this.path = path;
            this.parent = iface;


        }

    public Plugin getPlugin(int indeks) {
        return plugins.get(indeks);
    }
    public void dodajPlugin(Plugin extension) {
         this.plugins.add(extension);
    }

    public ArrayList<PluginSpecification> getPlugini() {
        return pluginSpecifikacije;
    }

    public ArrayList<Plugin> getPlugins() {
        return plugins;
    }


    public void activate(int indeksIzTabele) {
        for (Plugin plugin : plugins) {
            if (this.getPlugin(indeksIzTabele).getId().intValue() == plugin.getId().intValue()) {
                parent.dodajAktiviraniPlugin((Integer)plugin.getId());
                plugin.activate();
            }
        }
    }

    public void deactivate(int indeksIzTabele) {
        for (Plugin plugin : plugins) {
            if (this.getPlugin(indeksIzTabele).getId().intValue() == plugin.getId().intValue()) {
                parent.ukloniAktiviraniPlugin((Integer)plugin.getId());
                plugin.deactivate();
            }
        }
    }
    public void aktiviratiIzKonteksta(ArrayList<Integer> aktiviraniPluginovi){
        for(Plugin p: this.plugins){

            for(Integer ID: aktiviraniPluginovi){
                if(p.getId().intValue() == ((Integer)ID).intValue()){
                    p.aktivirajIzKonteksta();
                }
            }
        }
    }

    public Plugin getActivatedPlugin(Integer ID){
        for (Plugin plugin:plugins){
            if(plugin.getId() == ID){
                return plugin;
            }
        }
        return null;
    }
    public void napuniRegistar(String pluginsPath){

        try {
            List<Path> jsonFiles = PluginSpecification.findSpecificationJsonFiles(pluginsPath);
            for (Path jsonFile : jsonFiles) {
                this.pluginSpecifikacije.add(PluginSpecification.ucitajSpecifikaciju(jsonFile.toString()));
            }
        } catch (IOException e2) {
            e2.printStackTrace();
        }

        //TODO: Prepraviti da ne bude hardkodovano nego putem neke metode ovo odraditi ili stogod...

        for (PluginSpecification pluginSpecification : this.pluginSpecifikacije) {
            switch (pluginSpecification.getName()) {
                case "DocumentHandler":
                    this.dodajPlugin(new DocumentHandler(pluginSpecification, parent));
                    break;
                case "WorkspaceHandler":
                    this.dodajPlugin(new WorkspaceHandler(pluginSpecification, parent));
                    break;
                case "ElementHandler":
                    this.dodajPlugin(new ElementHandler(pluginSpecification, parent));
                    break;
                case "HelpPlugin":
                    this.dodajPlugin(new HelpPlugin(pluginSpecification,parent));
                    break;
            }
        }
    }

}


package rs.ac.singidunum.core.ui.pluginManager.plugin;

public interface Plugin {

    public void activate();
    public void deactivate();
    public Integer getId();

    public void aktivirajIzKonteksta();
    public void sacuvajKontekst();

    public PluginSpecification getPluginSpecification();
}

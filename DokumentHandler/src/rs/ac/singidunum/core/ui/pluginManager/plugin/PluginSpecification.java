package rs.ac.singidunum.core.ui.pluginManager.plugin;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PluginSpecification {

    private Integer id;
    private String name;
    private ArrayList<Author> autori = new ArrayList<>();
    private String version;
    private String coreVersion;
    private String category;
    private String description;
    private String webPage;
    private ArrayList<Dependency> dependencies = new ArrayList<>();

    private String pluginContext;

    public PluginSpecification(Integer id, String name, ArrayList<Author> autori, String version, String coreVersion, String category, String description,
                               String webPage, ArrayList<Dependency> dependencies, String pluginContext) {
        this.id = id;
        this.name = name;
        this.autori = autori;
        this.version = version;
        this.coreVersion = coreVersion;
        this.category = category;
        this.description = description;
        this.webPage = webPage;
        this.dependencies = dependencies;
        this.pluginContext = pluginContext;
    }


    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Author> getAutori() {
        return autori;
    }

    public void setAutori(ArrayList<Author> autori) {
        this.autori = autori;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getCoreVersion() {
        return coreVersion;
    }

    public String getPluginContext() {
        return pluginContext;
    }

    public void setCoreVersion(String coreVersion) {
        this.coreVersion = coreVersion;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getWebPage() {
        return webPage;
    }

    public void setWebPage(String webPage) {
        this.webPage = webPage;
    }

    public ArrayList<Dependency> getDependencies() {
        return dependencies;
    }

    public void setDependencies(ArrayList<Dependency> dependencies) {
        this.dependencies = dependencies;
    }


    public static PluginSpecification ucitajSpecifikaciju(String specificationPath) {
        JSONParser parser = new JSONParser();

        try {
            // Read the JSON file
            FileReader reader = new FileReader(specificationPath);
            Object obj = parser.parse(reader);

            // Parse the JSON object
            if (obj instanceof JSONObject) {
                JSONObject jsonObject = (JSONObject) obj;

                // Now, you can extract fields from the JSON and construct your object
                String id =  jsonObject.get("id").toString();
                String name = (String) jsonObject.get("name");
                ArrayList<Author> authors = (ArrayList<Author>) jsonObject.get("authors");
                String version = (String) jsonObject.get("version");
                String core_version = (String) jsonObject.get("core_version");
                String category = (String) jsonObject.get("category");
                String description = (String) jsonObject.get("description");
                String web_page = (String) jsonObject.get("web_page");
                ArrayList<Dependency> dependencies = (ArrayList<Dependency>) jsonObject.get("dependencies");
                String pluginContext = jsonObject.get("context").toString();


                // Create and return your PluginSpecification object
                return new PluginSpecification(Integer.parseInt(id), name,authors, version, core_version, category, description, web_page, dependencies,pluginContext);
            } else {
                // Handle the case where the JSON doesn't represent an object
                throw new IllegalArgumentException("Invalid JSON format: JSON object expected");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        // Handle exceptions or return null if necessary
        return null;
    }



    public static List<Path> findSpecificationJsonFiles(String directoryPath) throws IOException {
        List<Path> jsonFiles = new ArrayList<>();

        Path directory = Paths.get(directoryPath);

        Files.walkFileTree(directory, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                if (file.getFileName().toString().equals("specification.json")) {
                    jsonFiles.add(file);
                }
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
                // Handle file visit failure, if necessary
                return FileVisitResult.CONTINUE;
            }
        });

        return jsonFiles;
    }
}
package rs.ac.singidunum.core.ui.pluginManager;

import rs.ac.singidunum.core.ui.pluginManager.plugin.PluginRegistry;
import rs.ac.singidunum.core.ui.pluginManager.plugin.PluginSpecification;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.ArrayList;

public class PluginManager extends JDialog {
    private JTable tabela = new JTable();
    private PluginRegistry pluginRegistry;


    public PluginManager(Frame owner, String title, JTable tabela, PluginRegistry pluginRegistry) {
        super(owner, title);
        this.tabela = tabela;
        this.pluginRegistry = pluginRegistry;
        setSize(400, 200);
    }

 public void init(){

        JToolBar toolBar = new JToolBar();
        JButton dugmeActivate = new JButton("Activate");
        JButton dugmeDeactivate = new JButton("Deactivate");

        dugmeActivate.addActionListener(e->{
            this.pluginRegistry.activate(tabela.getSelectedRow());

        });

        dugmeDeactivate.addActionListener(e->{
            this.pluginRegistry.deactivate(tabela.getSelectedRow());
        });

        toolBar.add(dugmeActivate);
        toolBar.add(dugmeDeactivate);


        this.getContentPane().add(tabela, BorderLayout.CENTER);
        this.getContentPane().add(toolBar, BorderLayout.NORTH);

        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }
}

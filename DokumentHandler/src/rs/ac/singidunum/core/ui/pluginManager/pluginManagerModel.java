package rs.ac.singidunum.core.ui.pluginManager;

import rs.ac.singidunum.core.ui.pluginManager.plugin.PluginSpecification;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;

public class pluginManagerModel extends AbstractTableModel {

    ArrayList<PluginSpecification> plugins = new ArrayList<PluginSpecification>();

    public  pluginManagerModel(ArrayList<PluginSpecification> plugins){
        this.plugins = plugins;
    }

    public pluginManagerModel() {

    }

    @Override
    public int getRowCount() {
        return this.plugins.size();
    }

    @Override
    public int getColumnCount() {
        //Ovo treba iz meta da izvlaci
        return 5;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if(columnIndex == 0) {
            return this.plugins.get(rowIndex).getId();
        }
        if(columnIndex == 1) {
            return this.plugins.get(rowIndex).getAutori();
        }
        if(columnIndex == 2) {
            return this.plugins.get(rowIndex).getDescription();
        }
        if(columnIndex == 3) {
            return this.plugins.get(rowIndex).getVersion();
        }
//        if(columnIndex == 4) {
//            return this.plugins.get(rowIndex).isActivated();
//        }
        return null;
    }
}




package rs.ac.singidunum.core.ui;

import rs.ac.singidunum.core.ui.pluginManager.plugin.Plugin;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.JSONObject;
import org.json.simple.parser.*;
import org.json.simple.parser.ParseException;
import rs.ac.singidunum.config.Configuration;
import rs.ac.singidunum.core.ui.pluginManager.PluginManager;
import rs.ac.singidunum.core.ui.pluginManager.plugin.Author;
import rs.ac.singidunum.core.ui.pluginManager.plugin.Dependency;
import rs.ac.singidunum.core.ui.pluginManager.plugin.PluginRegistry;
import rs.ac.singidunum.core.ui.pluginManager.plugin.PluginSpecification;
import rs.ac.singidunum.core.ui.pluginManager.pluginManagerModel;
import rs.ac.singidunum.plugins.DocumentHandler.DocumentHandler;
import rs.ac.singidunum.plugins.ElementHandler.ElementHandler;
import rs.ac.singidunum.plugins.workspaceHandler.WorkspaceHandler;


import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.*;
import java.nio.file.Path;
import java.util.*;
import java.util.List;


public class IntegrativnaKomponenta extends JFrame{


    private JMenuBar menuBar = new JMenuBar();
    private Configuration configuration;
    private PluginRegistry pluginRegistry;

    private ArrayList<Integer> aktiviraniPluginovi = new ArrayList<>();

    private IntegrativnaKomponenta frame = this;

    public IntegrativnaKomponenta(Configuration configuration){
        super(configuration.getTitle());
        super.setSize(configuration.getWidth(),configuration.getHeight());
        this.configuration = configuration;
    }

    public void setPluginRegistry(PluginRegistry pluginRegistry) {
        this.pluginRegistry = pluginRegistry;
    }

    public void stampaj(){
//        System.out.println(this.aktiviraniPluginovi.get(0));
    }
    public void dodajAktiviraniPlugin(Integer indeks) {
        if(!(this.aktiviraniPluginovi.contains(indeks))) {
            this.aktiviraniPluginovi.add(indeks);
        }
    }

    public boolean proveraAktivacije(Integer ID){
        if(this.aktiviraniPluginovi.contains(ID)){
            return true;
        }
        return false;
    }
    public void ukloniAktiviraniPlugin(Integer indeks) {
        if(this.aktiviraniPluginovi.contains(indeks)) {
            this.aktiviraniPluginovi.remove(indeks);
        }
    }



    public ArrayList<Integer> getAktiviraniPluginovi() {
        return aktiviraniPluginovi;
    }


    public static Configuration loadConfiguration(JSONObject jsonConfig) throws IOException, ParseException {

        try {
            String title = (String) jsonConfig.get("title");
            String coreVersion = (String) jsonConfig.get("core_version");
            String icon = (String) jsonConfig.get("icon");
            Dictionary<String, Integer> windowSizeDict  = new Hashtable<>();

            JSONObject windowSize = (JSONObject) jsonConfig.get("window_size");
            int width = ((Long) windowSize.get("width")).intValue();
            int height = ((Long) windowSize.get("height")).intValue();
            windowSizeDict.put("width", width);
            windowSizeDict.put("height", height);
            JSONArray installedPluginsArray = (JSONArray) jsonConfig.get("installed_plugins");
            ArrayList<Object> installedPlugins = new ArrayList<>();
            for (int i = 0; i < installedPluginsArray.size(); i++) {
                installedPlugins.add((Object) installedPluginsArray.get(i));
            }

            String pluginsPath = (String) jsonConfig.get("plugins_path");
            String context = (String) jsonConfig.get("context");

            return new Configuration(title, coreVersion, icon, windowSizeDict, installedPlugins, pluginsPath, context);
        } catch (Exception e) {
            // Handle any potential exceptions
            e.printStackTrace();
            return null;  // Return a default configuration or handle the error as needed
        }
    }

    private void ucitajKontekst(String specificationPath) {
        JSONParser parser = new JSONParser();
        ArrayList<Long> zaKonverziju = new ArrayList<>();

        try {
            // Read the JSON file
            FileReader reader = new FileReader(specificationPath);
            Object obj = parser.parse(reader);

            // Parse the JSON object
            if (obj instanceof JSONObject) {
                JSONObject jsonObject = (JSONObject) obj;

                // Now, you can extract fields from the JSON and construct your object
                zaKonverziju = (ArrayList<Long>) jsonObject.get("aktiviraniPluginovi");

                for(Long l : zaKonverziju){
                    this.aktiviraniPluginovi.add(l.intValue());
                }

            } else {
                // Handle the case where the JSON doesn't represent an object
                throw new IllegalArgumentException("Invalid JSON format: JSON object expected");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
    public void sacuvajKontekst(){

        for(Integer id: this.aktiviraniPluginovi){
            (this.pluginRegistry.getActivatedPlugin(id)).sacuvajKontekst();
        }

        JSONObject obj = new JSONObject();

        // Dodajemo ArrayList u JSON objekat
        JSONArray jsonListaBrojeva = new JSONArray();
        jsonListaBrojeva.addAll(this.aktiviraniPluginovi);
        obj.put("aktiviraniPluginovi", jsonListaBrojeva);



        // Pisanje JSON objekta u fajl
        try (FileWriter file = new FileWriter(this.configuration.getContext())) {
            file.write(obj.toJSONString());
            System.out.println("Podaci su uspešno sačuvani u JSON fajl.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Plugin getActivatedPlugin(Integer ID){
        return this.pluginRegistry.getActivatedPlugin(ID);
    }

        public void init () {
            this.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent e) {
                    int odgovor = JOptionPane.showConfirmDialog(frame,
                            "Da li ste sigurni da želite zatvoriti prozor?",
                            "Potvrda zatvaranja",
                            JOptionPane.YES_NO_OPTION);

                    if (odgovor == JOptionPane.YES_OPTION) {
                        frame.sacuvajKontekst();
                        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                    } else {
                        // Ako je odgovor "No", sprečavamo zatvaranje prozora
                        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
                    }
                }
            });

            this.ucitajKontekst(configuration.getContext());
            pluginRegistry.napuniRegistar(configuration.getPluginsPath());


            JMenu pluginMenu = new JMenu("Plugin");



            JMenuItem pluginManager = new JMenuItem("Plugin Manager");

            pluginMenu.add(pluginManager);
            menuBar.add(pluginMenu);

            //Dodati otvaranje plugin managera
            pluginManager.addActionListener(e -> {

                PluginManager pluginManagerDijalog = new PluginManager(this, "plugini", new JTable(new pluginManagerModel(pluginRegistry.getPlugini())), pluginRegistry);

                pluginManagerDijalog.init();

            });


            pluginRegistry.aktiviratiIzKonteksta(this.aktiviraniPluginovi);
            this.getContentPane().add(menuBar, BorderLayout.NORTH);

            this.setLocationRelativeTo(null);
            this.setVisible(true);
            this.revalidate();
            this.repaint();
        }

        public void addMenuItem(JMenu meni){
            this.menuBar.add(meni);
            this.repaint();
            this.revalidate();
        }
        public void removeMenuItem(JMenu meni){
            this.menuBar.remove(meni);
            this.repaint();
            this.revalidate();
        }

    }






package rs.ac.singidunum.core.ui;

import javax.swing.*;
import java.awt.*;

public class ExceptionHandler {

    public static void printException(String title, String tekst, JFrame parent, Dimension dimension){
        JDialog dijalog = new JDialog(parent,title);
        dijalog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        dijalog.setLayout(new BorderLayout());
        dijalog.setSize(dimension);
        JButton buttonOK = new JButton("Ok");
        buttonOK.addActionListener(o->{
            dijalog.dispose();
        });
        dijalog.add(new JLabel(tekst),BorderLayout.CENTER);
        dijalog.add(buttonOK,BorderLayout.SOUTH);
        dijalog.setLocationRelativeTo(parent);
        dijalog.setVisible(true);
        dijalog.setVisible(true);
    }
}

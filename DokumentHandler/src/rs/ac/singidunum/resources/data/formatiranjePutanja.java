package rs.ac.singidunum.resources.data;

import java.io.File;

public class formatiranjePutanja {

    public static String napraviPutanju(String staSeMenja, String saCimMenjamo){
        if(staSeMenja.isEmpty()){
            return null;
        }
        String [] pocetnaPutanja;
        if(File.separator.equals("/")){
            pocetnaPutanja = staSeMenja.split(File.separator);
        }else{
            pocetnaPutanja = staSeMenja.split("\\\\");
        }

        String [] elementi = saCimMenjamo.split("/");
        String putanja = "";


        pocetnaPutanja[pocetnaPutanja.length - 2] = elementi[elementi.length-2];
        pocetnaPutanja[pocetnaPutanja.length - 1] = elementi[elementi.length-1];

        for(int i = 0; i < pocetnaPutanja.length; i++){
            if(i < pocetnaPutanja.length-1) {
                putanja = putanja + pocetnaPutanja[i] + File.separator;
            }else{
                putanja = putanja + pocetnaPutanja[i];
            }
        }
        return putanja;
    }
}
